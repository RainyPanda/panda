# Panda
A simple command line interface made with Rust. So far, it has very few commands but they include:
- `hash --INPUT` that hashes a given string using SHA-256.
- `--feedback --input` that will someday be used to send feedback, but currently just repeats the feedback with a message saying it was not sent.
- Functions to both create and read a text file, not yet integrated into the CLI with commands.