use clap::{Parser, Subcommand};
use sha2::{Sha256, Digest};

use std::fs;
use std::fs::File;
use std::os::windows::prelude::*;

/* https://docs.rs/clap/latest/clap/_derive/_tutorial/#quick-start */

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,

    #[arg(short, long)]
    feedback: Option<String>,
}

#[derive(Subcommand)]
enum Commands {
    /// Hashes a given string in SHA-256, do not rely on it to be secure
    Hash {
        #[arg(long)]
        input: String,
    },
}

fn main() {
    println!("{:?}", read_file());
    let cli = Cli::parse();

    if let Some(feedback) = cli.feedback.as_deref() {
        println!("Your feedback was {}", feedback);
        println!("This feedback is not sent anywhere (yet), it's just theoretical feedback.")
    }

    match &cli.command {
        Some(Commands::Hash { input }) => {
            hash(input.to_string());
        }

        None => {}
    }
}

fn hash(input: String) {
    let mut hasher = Sha256::new();
    let data = input.as_bytes();
    hasher.update(data);
    let hash = hasher.finalize();
    let hash = format!("{:x}", hash);
    println!("{}", hash);
}

fn make_file() -> std::io::Result<()> {
    let f = File::create("hello.txt");
    let _handle = f?.as_raw_handle();

    Ok(())
}

fn read_file() -> std::io::Result<String> {
    let content = fs::read_to_string("logs.txt");
    content
}